'use strict';

describe('Service: readService', function () {

  // load the service's module
  beforeEach(module('sirTp6App'));

  // instantiate service
  var readService;
  beforeEach(inject(function (_readService_) {
    readService = _readService_;
  }));

  it('should do something', function () {
    expect(!!readService).toBe(true);
  });

});
