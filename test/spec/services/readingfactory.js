'use strict';

describe('Service: readingFactory', function () {

  // load the service's module
  beforeEach(module('sirTp6App'));

  // instantiate service
  var readingFactory;
  beforeEach(inject(function (_readingFactory_) {
    readingFactory = _readingFactory_;
  }));

  it('should do something', function () {
    expect(!!readingFactory).toBe(true);
  });

});
