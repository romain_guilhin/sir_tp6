'use strict';

describe('Service: writingFactory', function () {

  // load the service's module
  beforeEach(module('sirTp6App'));

  // instantiate service
  var writingFactory;
  beforeEach(inject(function (_writingFactory_) {
    writingFactory = _writingFactory_;
  }));

  it('should do something', function () {
    expect(!!writingFactory).toBe(true);
  });

});
