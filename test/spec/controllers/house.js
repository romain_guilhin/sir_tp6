'use strict';

describe('Controller: HouseCtrl', function () {

  // load the controller's module
  beforeEach(module('sirTp6App'));

  var HouseCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HouseCtrl = $controller('HouseCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(HouseCtrl.awesomeThings.length).toBe(3);
  });
});
