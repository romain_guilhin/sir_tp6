# Compte Rendu #

### Configuration ###

* Cloner la branche master
* Lancer grunt serve
* Application disponible à l'adresse localhost:9000/#/

### Application ###

* L'Application permet d'afficher, d'ajouter et de supprimer des personnes, maisons, chauffages et appareils electrique.
* Le dossier "app\scripts\controllers" contient la liste des differents controllers.
* Le dossier "app\scripts\services" contient la factory "readingFactory" permettant de faire des appelles à l'API en GET ainsi que la factory "writingFactory" permettant des appelles à l'API en POST.
* Les vues sont contenues dans le dossier "app\views".

### Controllers ###
#### Tout les controllers possèdent les méthodes suivantes: ####
* La méthode "$scope.fetch" qui est appellée au chargement de la page est permet, avec l'aide du service "readingFactory", de récupérer dans le scope les valeurs de la BDD.
* Les méthodes "$scope.delete" et "$scope.create" qui permettent de supprimer et de créer une personne, maison, chauffage et appareils electrique en utilisant le service "writingFactory".