'use strict';

/**
 * @ngdoc service
 * @name sirTp6App.readingFactory
 * @description
 * # readingFactory
 * Factory in the sirTp6App.
 */
angular.module('sirTp6App')
  .factory('readingFactory', function ($resource) {
    // Service logic
    return {
    get: function(route) {
      var url = "http://localhost:9000/rest/";
      url = url + route;
      console.log(url);
      return $resource(url, {}, {
        query: { method: 'GET', params: {}, isArray: true }
      });

    }
  }
  });
