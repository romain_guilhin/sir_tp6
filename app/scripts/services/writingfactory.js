'use strict';

/**
 * @ngdoc service
 * @name sirTp6App.writingFactory
 * @description
 * # writingFactory
 * Factory in the sirTp6App.
 */
angular.module('sirTp6App')
  .factory('writingFactory', function ($resource) {
    return {
      post: function(route) {
        var url = "http://localhost:9000/rest/";
        url = url + route;
        return $resource(url, {}, {
          query: { method: 'POST', params: {}}
        });
      },
      createHeater: function(data) {
        var url = "http://localhost:9000/rest/";
        url = url + data.url;
        return $resource(url, {}, {
          query: { method: 'POST', params: {"power": data.power, "heater-house-id": data.houseId}, isArray: true}
        });
      },
      createDevice: function(data) {
        var url = "http://localhost:9000/rest/";
        url = url + data.url;
        return $resource(url, {}, {
          query: { method: 'POST', params: {"power": data.power, "device-house-id": data.houseId}, isArray: true}
        });
      },
      createHouse: function(data) {
        var url = "http://localhost:9000/rest/";
        url = url + data.url;
        return $resource(url, {}, {
          query: { method: 'POST', params: {"rooms": data.rooms, "size": data.size, "housePerson": data.housePerson, "houseHeater": data.houseHeater, "houseDevice": data.houseDevice}, isArray: true}
        });
      },
      createPerson: function(data) {
        var url = "http://localhost:9000/rest/";
        url = url + data.url;
        return $resource(url, {}, {
          query: { method: 'POST', params: {"firstName": data.firstName, "name": data.name, "mail": data.mail}, isArray: true}
        });
      }
    };
  });
