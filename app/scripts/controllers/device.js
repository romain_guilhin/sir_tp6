'use strict';

/**
 * @ngdoc function
 * @name sirTp6App.controller:DeviceCtrl
 * @description
 * # DeviceCtrl
 * Controller of the sirTp6App
 */
angular.module('sirTp6App')
  .controller('DeviceCtrl', ['$scope', 'readingFactory', 'writingFactory', function ($scope, readingFactory, writingFactory) {
    var _id = 0;
    var _list = [];
    var _houseList = [];
    var _power = 0;
    var _houseId = 0;
    $scope.detail = {
      id: function(newId) {
        return arguments.length ? (_id = newId) : _id;
      },
      list: function(){
        return _list;
      },
      houseList: function(){
        return _houseList;
      },
      power: function(p){
        return arguments.length ? (_power = p) : _power;
      },
      houseId: function(id){
        return arguments.length ? (_houseId = id) : _houseId;
      }
    };

    $scope.fetch = function() {
      readingFactory.get("device").query(function(data){
        _list = [];
        angular.forEach(data, function(key, value) {
          //console.log(key.id);
          if(key.house == null){
            this.push({"id": key.id, "power": key.power, "house": ""});
          }else{
            this.push({"id": key.id, "power": key.power, "house": key.house.id})
          }
        }, _list);
        $("#loader").hide();
      });

      readingFactory.get("house").query(function(data){
        _houseList = [];
        console.log(data);
        angular.forEach(data, function(key, value) {
          console.log(key.id);
          this.push(key.id)
        }, _houseList);
      });

    };

    $scope.delete = function(id) {
      $("#loader").show();
      $scope.heaterDetails = writingFactory.post("device/delete-device/" + id).query(function(data){
      $scope.fetch();
    });

  };

  $scope.create = function() {
    $("#loader").show();
    $scope.heaterDetails = writingFactory.createDevice({url: "device/create-device", power: _power, houseId: _houseId}).query(function(data){
    $scope.fetch();
  });
};

  }]);
