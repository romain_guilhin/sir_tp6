'use strict';

/**
* @ngdoc function
* @name sirTp6App.controller:PersonCtrl
* @description
* # PersonCtrl
* Controller of the sirTp6App
*/
angular.module('sirTp6App')
.controller('PersonCtrl', ['$scope', 'readingFactory', 'writingFactory', function ($scope, readingFactory, writingFactory) {
  var _id = 0;
  var _list = [];
  var _name = '';
  var _firstname = '';
  var _mail = '';
  var _friends = [];
  var _houses = [];

  $scope.detail = {
    id: function(newId) {
      return arguments.length ? (_id = newId) : _id;
    },
    list: function(){
      return _list;
    },
    name: function(n){
      return arguments.length ? (_name = n) : _name;
    },
    firstname: function(n){
      return arguments.length ? (_firstname = n) : _firstname;
    },
    mail: function(n){
      return arguments.length ? (_mail = n) : _mail;
    },
    friends: function(){
      return _friends;
    },
    houses: function(){
      return _houses;
    }
  };

  $scope.fetch = function() {
    readingFactory.get("person").query(function(data){
      _list = [];
      angular.forEach(data, function(key, value) {
        var t = '';
        angular.forEach(key.myHouses, function(key, value) {
          t += key.id + " / ";
        });
        t = t.slice(0,-2);
        var t2 = '';
        angular.forEach(key.myFriends, function(key, value) {
          t2 += key.firstName + " " + key.name  + " / ";
        });
        t2 = t2.slice(0,-2);
        //console.log(key.id);
        this.push({"id": key.id, "firstName": key.firstName, "name": key.name, "mail": key.mail, "myHouses": t, "myFriends": t2})
      }, _list);
      $("#loader").hide();
    });

  };

  $scope.delete = function(id) {
    $("#loader").show();
    $scope.heaterDetails = writingFactory.post("person/delete-user/" + id).query(function(data){
    $scope.fetch();
  });

};

$scope.create = function() {
  $("#loader").show();
$scope.heaterDetails = writingFactory.createPerson({url: "person/create-person", firstName: _firstname,  name: _name, mail: _mail}).query(function(data){
  $scope.fetch();
});
};
}]);
