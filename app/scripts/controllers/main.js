'use strict';

/**
 * @ngdoc function
 * @name sirTp6App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sirTp6App
 */
angular.module('sirTp6App')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
