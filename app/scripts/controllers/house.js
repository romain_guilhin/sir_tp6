'use strict';

/**
 * @ngdoc function
 * @name sirTp6App.controller:DeviceCtrl
 * @description
 * # DeviceCtrl
 * Controller of the sirTp6App
 */
angular.module('sirTp6App')
  .controller('HouseCtrl', ['$scope', 'readingFactory', 'writingFactory', function ($scope, readingFactory, writingFactory) {
    var _id = 0;
    var _list = [];
    var _personList = [];
    var _heaterList = [];
    var _deviceList = [];
    var _personId = 0;
    var _heaterId = 0;
    var _deviceId = 0;
    var _rooms = 0;
    var _size = 0;
    $scope.detail = {
      id: function(newId) {
        return arguments.length ? (_id = newId) : _id;
      },
      list: function(){
        return _list;
      },
      personList: function(){
        return _personList;
      },
      heaterList: function(){
        return _heaterList;
      },
      deviceList: function(){
        return _deviceList;
      },
      rooms: function(p){
        return arguments.length ? (_rooms = p) : _rooms;
      },
      size: function(s){
        return arguments.length ? (_size = s) : _size;
      },
      personId: function(id){
        return arguments.length ? (_personId = id) : _personId;
      },
      heaterId: function(id){
        return arguments.length ? (_heaterId = id) : _heaterId;
      },
      deviceId: function(id){
        return arguments.length ? (_deviceId = id) : _deviceId;
      }
    };

    $scope.fetch = function() {
      readingFactory.get("house").query(function(data){
        _list = [];
        angular.forEach(data, function(key, value) {
          var t = '';
          angular.forEach(key.myDevices, function(key, value) {
            t += key.id + " / ";
          });
          t = t.slice(0,-2);
          var t2 = '';
          angular.forEach(key.myHeaters, function(key, value) {
            t2 += key.id + " / ";
          });
          t2 = t2.slice(0,-2);
          //console.log(key.id);
          this.push({"id": key.id, "rooms": key.rooms, "size": key.size, "myDevices": t, "myHeaters": t2,  "person": key.person.firstName + " " +key.person.name})
        }, _list);
        $("#loader").hide();
      });

      readingFactory.get("person").query(function(data){
        _personList = [];
        console.log(data);
        angular.forEach(data, function(key, value) {
          console.log(key.id);
          this.push({"id": key.id, "pers": key.firstName + " " +key.name});
        }, _personList);
      });

      readingFactory.get("heater").query(function(data){
        _heaterList = [];
        console.log(data);
        angular.forEach(data, function(key, value) {
          console.log(key.id);
          this.push({"id": key.id});
        }, _heaterList);
      });

      readingFactory.get("device").query(function(data){
        _deviceList = [];
        console.log(data);
        angular.forEach(data, function(key, value) {
          console.log(key.id);
          this.push({"id": key.id});
        }, _deviceList);
      });
    };

    $scope.delete = function(id) {
      $("#loader").show();
      $scope.heaterDetails = writingFactory.post("house/delete-house/" + id).query(function(data){
      $scope.fetch();
    });

  };

  $scope.create = function() {
    $("#loader").show();
    $scope.heaterDetails = writingFactory.createHouse({url: "house/create-house", rooms: _rooms, size: _size, housePerson: _personId,  houseHeater: _heaterId, houseDevice: _deviceId}).query(function(data){
    $scope.fetch();
  });
};

  }]);
