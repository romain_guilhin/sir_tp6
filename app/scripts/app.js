'use strict';

/**
 * @ngdoc overview
 * @name sirTp6App
 * @description
 * # sirTp6App
 *
 * Main module of the application.
 */
angular
  .module('sirTp6App', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/person', {
        templateUrl: 'views/person.html',
        controller: 'PersonCtrl',
        controllerAs: 'person'
      })
      .when('/house', {
        templateUrl: 'views/house.html',
        controller: 'HouseCtrl',
        controllerAs: 'house'
      })
      .when('/heater', {
        templateUrl: 'views/heater.html',
        controller: 'HeaterCtrl',
        controllerAs: 'heater'
      })
      .when('/device', {
        templateUrl: 'views/device.html',
        controller: 'DeviceCtrl',
        controllerAs: 'device'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
